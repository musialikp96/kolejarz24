import { Component } from '@angular/core';
import { AdminService } from '../admin.service';
import { Auth } from '../../authentication/auth';
import { FormBuilder, Validators, FormGroup, FormControl} from '@angular/forms';
import { Player } from '../../models/Player';
@Component({
  selector: 'add-player',

  styleUrls: [  './add-player.component.less'],
  templateUrl: './add-player.component.html'
})
export class AddPlayerComponent {
  constructor(private fb: FormBuilder, private _adminService: AdminService){
    this.form = fb.group({
      name: this.name,
      team: this.team,
      lastName: this.lastName,
      role: this.role,
      phoneNumber: this.phoneNumber,
      address: this.address,
      firstClub: this.firstClub,
      matches: this.matches,
      minutes: this.minutes,
      goals: this.goals,
      yellows: this.yellows,
      reds: this.reds
    });
  }
  player: Player;
  form: FormGroup;
  name: FormControl = new FormControl('', Validators.required);
  team: FormControl = new FormControl('Seniorzy', Validators.required);
  lastName: FormControl = new FormControl('', Validators.required);
  role: FormControl = new FormControl('Bramkarz', Validators.required);
  phoneNumber: FormControl = new FormControl('', Validators.required);
  address: FormControl = new FormControl('', Validators.required);
  firstClub: FormControl = new FormControl('', Validators.required);
  matches: FormControl = new FormControl('', Validators.required);
  minutes: FormControl = new FormControl('', Validators.required);
  goals: FormControl = new FormControl('', Validators.required);
  yellows: FormControl = new FormControl('', Validators.required);
  reds: FormControl = new FormControl('', Validators.required);


  add(){
    let data = this.form.value;
    this.player = {
      id: null,
      name: data['name'],
      team: this.getTeamNumber(data['team']),
      lastName: data['lastName'],
      role: data['role'],
      phoneNumber: data['phoneNumber'],
      address: data['address'],
      firstClub: data['firstClub'],
      matches: data['matches'],
      minutes: data['minutes'],
      goals: data['goals'],
      yellows: data['yellows'],
      reds:data['reds'],
      photoUrl: null
    }
    this._adminService.addPlayer(this.player).subscribe();
  }
  getTeamNumber(value:string): number{
    let number;
    switch(value){
      case "Seniorzy": {
        number=1;
        break;
      }
      case "Mlodzi": {
        number=2;
        break;
      }
    }
    return number;
  }
}
