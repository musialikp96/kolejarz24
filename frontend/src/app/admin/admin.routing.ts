import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';

import { AdminComponent } from './admin.component';
import { AdminMainComponent } from './admin-main/admin-main.component';
import { AddPlayerComponent } from './add-player/add-player.component';
import { PlayersListComponent } from './players-list/players-list.component';

const adminRoutes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: 'dodaj-zawodnika',
        component: AddPlayerComponent,
      },
      {
        path: 'lista-zawodnikow',
        component: PlayersListComponent,
      }
    ]
  }
];

export const AdminRouting: ModuleWithProviders = RouterModule.forChild(adminRoutes);
