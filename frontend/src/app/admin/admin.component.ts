import { Component } from '@angular/core';

@Component({
  selector: 'admin',

  styleUrls: [  './admin.component.less'],
  templateUrl: './admin.component.html'
})
export class AdminComponent {
  constructor(){}
  activeLi:number;
  toggleActive(id){
      this.activeLi = id;
  }
  isActive(id){
    if(this.activeLi==id) return true;
    return false;
  }
}
