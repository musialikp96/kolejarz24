import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Player } from '../models/Player';
import { Observable } from 'rxjs/Rx';
// import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AdminService {
  constructor(public http: Http) { }

  baseUrl: string = 'http://localhost:4000';

  addPlayer(player: Player): Observable<any>{

    let url = this.baseUrl + '/common/addPlayer';
    let body = JSON.stringify(player);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, body, options).map(this.extractData).catch(this.handleError);
  }

  getAllPlayers(): Observable<any>{

    let url = this.baseUrl + '/common/getAllPlayers';
    return this.http.get(url).map(this.extractData).catch(this.handleError);
  }
  
  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
}
