import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AdminComponent } from './admin.component';
import { AdminRouting } from './admin.routing';
import { AdminService } from './admin.service';

import { AdminMainComponent } from './admin-main/admin-main.component';
import { AddPlayerComponent } from './add-player/add-player.component';
import { PlayersListComponent } from './players-list/players-list.component';

import { LoadingCircleModule } from '../animations/loading-circle/loading-circle.module.ts';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRouting,
    LoadingCircleModule
  ],
  declarations: [
    AdminComponent,
    AdminMainComponent,
    AddPlayerComponent,
    PlayersListComponent
  ],
  exports: [
    AdminComponent
  ],
  providers: [
    AdminService
  ]
})
export class AdminModule {}
