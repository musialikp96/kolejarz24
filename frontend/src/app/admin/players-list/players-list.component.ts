import { Component, OnInit } from '@angular/core';
import { TeamService } from '../../home/content/team/team.service';
import { AdminService } from '../admin.service';
import { Player } from '../../models/Player';

@Component({
  selector: 'players-list',

  styleUrls: [  './players-list.component.less'],
  templateUrl: './players-list.component.html'
})
export class PlayersListComponent implements OnInit{
  constructor(private _teamService: TeamService, private _adminService: AdminService){}
  players: Player[]=[];
  id:number=1;

  ngOnInit(){
    this.getTeam(this.id);
    // this._adminService.getAllPlayers().subscribe(res=>{
    //   this.players=res;
    // })
  }
  getTeam(id){
    this._teamService.getTeam(id).subscribe(res=>{
      this.players=res;
    })
  }
}
