export class Player{
  id: number;
  team: number; //1 - seniorka, 2 - trampkarze
  name: string;
  lastName: string;
  role: string;
  phoneNumber: string;
  address: string;
  firstClub: string;
  matches: number;
  minutes: number;
  goals: number;
  yellows: number;
  reds: number;
  photoUrl: string

  constructor(id, team, name, lastName, role, phoneNumber, address, firstClub, matches, minutes, goals, yellows, reds, photoUrl) {
      this.id = id || null;
      this.team = team || null;
      this.name = name || null;
      this.lastName = lastName || null;
      this.role = role || null;
      this.phoneNumber = phoneNumber || null;
      this.address = address || null;
      this.firstClub = firstClub || null;
      this.matches = matches || null;
      this.minutes = minutes || null;
      this.goals = goals || null;
      this.yellows = yellows || null;
      this.reds = reds || null;
      this.photoUrl = photoUrl || null;
  }
}
