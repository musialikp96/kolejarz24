import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HomeService {
  constructor(public http: Http) { }

  baseUrl: string = 'http://localhost:4000';

  getTable(): Observable<any>{

    let url = this.baseUrl + '/common/getTable';

    return this.http.get(url).map(this.extractData).catch(this.handleError);
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
}
