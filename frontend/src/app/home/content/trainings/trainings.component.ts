import { Component } from '@angular/core';
import { Animations } from '../../../animations/animations.ts';


@Component({
  selector: 'trainings',
  styleUrls: [ './trainings.component.less' ],
  templateUrl: './trainings.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class TrainingsComponent {

}
