import { Component } from '@angular/core';
import { Animations } from '../../../animations/animations.ts';


@Component({
  selector: 'planner',
  styleUrls: [ './planner.component.less' ],
  templateUrl: './planner.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class PlannerComponent {

}
