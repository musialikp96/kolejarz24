import { Component } from '@angular/core';
import { Animations } from '../../../animations/animations.ts';


@Component({
  selector: 'home-page',
  styleUrls: [ './home-page.component.less' ],
  templateUrl: './home-page.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class HomePageComponent {

}
