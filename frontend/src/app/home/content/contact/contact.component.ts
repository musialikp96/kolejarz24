import { Component } from '@angular/core';
import { Animations } from '../../../animations/animations.ts';


@Component({
  selector: 'contact',
  styleUrls: [ './contact.component.less' ],
  templateUrl: './contact.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class ContactComponent {

}
