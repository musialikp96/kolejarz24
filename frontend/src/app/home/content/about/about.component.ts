import { Component } from '@angular/core';
import { Animations } from '../../../animations/animations.ts';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'about',
  styleUrls: [ './about.component.less' ],
  templateUrl: './about.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class AboutComponent {
  constructor(private route: ActivatedRoute) {  }
}
