import { Component, OnInit } from '@angular/core';
import { Animations } from '../../../animations/animations.ts';
import { ActivatedRoute, Router} from '@angular/router';
import { TeamService } from './team.service';

@Component({
  selector: 'team',
  styleUrls: [ './team.component.less' ],
  templateUrl: './team.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class TeamComponent implements OnInit{
  team:any;
  idTeam:number;
  goalkeepers:any[];
  defenders:any[];
  midfielders:any[];
  forwards:any[];
  loading: boolean;
  constructor(private route: ActivatedRoute, private router: Router, private _teamService: TeamService) {}

  ngOnInit(){
    this.router.events.subscribe((val) => {
        this.idTeam=this.route.snapshot.params['id'];
        this.getTeam(this.idTeam);
    });
  }

  getTeam(id){
    this.loading=true;
    this._teamService.getTeam(id).subscribe(res=>{
      this.team=res;
      this.divideGroups(res);
      this.loading=false;
    })
  }

  divideGroups(team){
    this.goalkeepers=[];
    this.defenders=[]
    this.midfielders=[];
    this.forwards=[];
    let g=0, d=0, m=0, f=0;
    for (let player of team){
      if(player.role=="Bramkarz"){
        this.goalkeepers[g++]=player;
      }
      if(player.role=="Obrońca"){
        this.defenders[d++]=player;
      }
      if(player.role=="Pomocnik"){
        this.midfielders[m++]=player;
      }
      if(player.role=="Napastnik"){
        this.forwards[f++]=player;
      }
    }
  }

  openPlayer(id){
    this.router.navigate(['home/player/'+id]);
  }
}
