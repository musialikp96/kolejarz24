import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TeamService } from '../team.service';
import { Animations } from '../../../../animations/animations.ts';

@Component({
  selector: 'player',
  styleUrls: [ './player.component.less' ],
  templateUrl: './player.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class PlayerComponent implements OnInit{
  constructor(private _teamService: TeamService, private route: ActivatedRoute){}

  id:number;
  player:any=[];

  ngOnInit(){
    this.id=this.route.snapshot.params['id'];
    this.getPlayer(this.id);
    window.scrollTo(0,0);
  }

  getPlayer(id){
    this._teamService.getPlayer(id).subscribe(player=>{
      this.player=player;
    })
  }
}
