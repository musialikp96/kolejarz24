import { Component } from '@angular/core';
import { Animations } from '../../../animations/animations.ts';


@Component({
  selector: 'news',
  styleUrls: [ './news.component.less' ],
  templateUrl: './news.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class NewsComponent {

}
