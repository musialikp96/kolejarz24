import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../home.service';
import { Animations } from '../../../animations/animations.ts';


@Component({
  selector: 'league',

  styleUrls: [ '../../../../assets/css/style.less', './league.component.less' ],
  templateUrl: './league.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class LeagueComponent implements OnInit{
  constructor(private _homeService: HomeService){}
  loading:boolean=true;
  table=[];
  ngOnInit(){
    this._homeService.getTable().subscribe(res=>{
      this.table=res;
      this.loading=false;
    });
  };
  isOurTeam(team){
    team=team.replace(/\s/g, '');
    if(team=="KSKOLEJARZ24KATOWICE") return true;
    return false;
  }
}
