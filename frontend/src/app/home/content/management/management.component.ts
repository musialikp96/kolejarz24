import { Component } from '@angular/core';
import { Animations } from '../../../animations/animations.ts';


@Component({
  selector: 'management',
  styleUrls: [ './management.component.less' ],
  templateUrl: './management.component.html',
  host: { '[@routeAnimation]': 'true' },
  animations: Animations.page
})
export class ManagementComponent {

}
