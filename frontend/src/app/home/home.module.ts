import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home.component';

import { NavbarComponent } from './navbar/navbar.component';

import { LeagueComponent } from './content/league/league.component';
import { TeamComponent } from './content/team/team.component';
import { AboutComponent } from './content/about/about.component';
import { PlayerComponent } from './content/team/player/player.component';
import { ContactComponent } from './content/contact/contact.component';
import { ManagementComponent } from './content/management/management.component';
import { TrainingsComponent } from './content/trainings/trainings.component';
import { NewsComponent } from './content/news/news.component';
import { PlannerComponent } from './content/planner/planner.component';
import { HomePageComponent } from './content/home-page/home-page.component';
import { LoginComponent } from './login/login.component';

import { HomeRouting } from './home.routing';
import { HomeService } from './home.service';
import { LoginService } from './login/login.service';
import { TeamService } from './content/team/team.service';
import { LoadingCircleModule } from '../animations/loading-circle/loading-circle.module.ts';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HomeRouting,
    LoadingCircleModule
  ],
  declarations: [
    HomeComponent,
    NavbarComponent,
    TeamComponent,
    AboutComponent,
    LeagueComponent,
    PlayerComponent,
    ContactComponent,
    ManagementComponent,
    TrainingsComponent,
    NewsComponent,
    PlannerComponent,
    HomePageComponent,
    LoginComponent

  ],
  exports: [
    HomeComponent,
    NavbarComponent
  ],
  providers: [
    HomeService,
    TeamService,
    LoginService
  ]
})
export class HomeModule {}
