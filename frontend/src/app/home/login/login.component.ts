import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl} from '@angular/forms';
import { LoginService } from './login.service';

@Component({
  selector: 'login',
  styleUrls: [ './login.component.less' ],
  templateUrl: './login.component.html'
})
export class LoginComponent {
  constructor(private _loginService: LoginService, fb: FormBuilder, private router:Router) {
    this.form = fb.group({
      username: this.username,
      password: this.password
    });
  }
  form: FormGroup;
  username: FormControl = new FormControl('admin', Validators.required);
  password: FormControl = new FormControl('', Validators.required);
  informations: string = null;

  log(){
    if(this.username.value=="admin")
      this.router.navigate(['admin']);
    else
      this.informations="zalogowany user"

  }
}
