import { Component } from '@angular/core';

@Component({
  selector: 'navbar',

  styleUrls: [ './navbar.component.less' ],
  templateUrl: './navbar.component.html'
})
export class NavbarComponent {
  isVisible:boolean=false;
  isHovered:boolean=false;
  isUserSettings:boolean=false;

  toggleMobileMenu(){
    this.isVisible=!this.isVisible;
  }
  toggleIsHovered(){
    this.isHovered=!this.isHovered;
  }
  toggleIsUserSettings(){
    this.isUserSettings=!this.isUserSettings;
  }

  isMobileMenuVisible(){
    return this.isVisible;
  }
  isHoveredMobileMenuOption(){
    return this.isHovered;
  }
  isUserSettingsOpened(){
    return this.isUserSettings;
  }
}
