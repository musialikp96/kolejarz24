import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';

import { HomeComponent } from './home.component';
import { TeamComponent } from './content/team/team.component';
import { AboutComponent } from './content/about/about.component';
import { ContactComponent } from './content/contact/contact.component';
import { PlayerComponent } from './content/team/player/player.component';
import { ManagementComponent } from './content/management/management.component';
import { TrainingsComponent } from './content/trainings/trainings.component';
import { LeagueComponent } from './content/league/league.component';
import { NewsComponent } from './content/news/news.component';
import { PlannerComponent } from './content/planner/planner.component';
import { HomePageComponent } from './content/home-page/home-page.component';

const homeRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: HomePageComponent,
      },
      {
        path: 'historia',
        component: AboutComponent,
      },
      {
        path: 'zarzad',
        component: ManagementComponent,
      },
      {
        path: 'kontakt',
        component: ContactComponent,
      },
      {
        path: 'druzyna/:id',
        component: TeamComponent,
      },
      {
        path: 'tabela',
        component: LeagueComponent,
      },
      {
        path: 'terminarz',
        component: PlannerComponent,
      },
      {
        path: 'player/:id',
        component: PlayerComponent,
      },
      {
        path: 'treningi',
        component: TrainingsComponent,
      },
      {
        path: 'aktualnosci',
        component: NewsComponent,
      }
    ]
  }
];

export const HomeRouting: ModuleWithProviders = RouterModule.forChild(homeRoutes);
