import { Routes, RouterModule } from '@angular/router';

import { DataResolver } from './app.resolver';

import { HomeComponent } from './home/home.component';
import { TeamComponent } from './home/content/team/team.component';
import { AboutComponent } from './home/content/about/about.component';
import { ContactComponent } from './home/content/contact/contact.component';
import { PlayerComponent } from './home/content/team/player/player.component';
import { ManagementComponent } from './home/content/management/management.component';
import { TrainingsComponent } from './home/content/trainings/trainings.component';
import { LeagueComponent } from './home/content/league/league.component';
import { NewsComponent } from './home/content/news/news.component';
import { PlannerComponent } from './home/content/planner/planner.component';
import { HomePageComponent } from './home/content/home-page/home-page.component';


import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';


export const ROUTES: Routes = [
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'user',
    component: UserComponent
  },
  {
    path: '',
    component: HomeComponent
  }
];
