import {style, animate, transition, state, trigger, keyframes} from '@angular/core';

export class Animations {
  static page = [
    trigger('routeAnimation', [
      state('*', style({opacity: '1'})),
      transition('void => *', [
        style({position: 'absolute'}),
        animate('600ms ease-in-out', keyframes([
          style({width:'100%', opacity: 0, offset: 0, transform:"translateX(-100%)"}),
          style({opacity: 1, offset: 0.9, transform:"translateX(0)"})
        ]))
    ]

      ),
      transition('* => void', [
        style({float:'right', opacity: '1'}),
      animate('600ms ease-in-out', keyframes([
        style({width:'100%', opacity: 1, offset: 0}),
        style({width:'100%', transform: "translateX(100%)", opacity: 0, offset: 0.9})
      ]))
    ])
    ])
  ];
}
