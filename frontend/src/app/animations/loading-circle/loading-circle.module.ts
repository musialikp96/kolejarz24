import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


import { LoadingCircle } from './loading-circle.ts';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoadingCircle
  ],
  exports: [
    LoadingCircle

  ]
})
export class LoadingCircleModule {}
