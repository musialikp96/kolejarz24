import { Component } from '@angular/core';

@Component({
  selector: 'loading-circle',
  styleUrls: [ './loading-circle.less' ],
  templateUrl: './loading-circle.html'
})
export class LoadingCircle {}
