import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { UserComponent } from './user.component';
import { UserRouting } from './user.routing';
import { UserService } from './user.service';
import { LoadingCircleModule } from '../animations/loading-circle/loading-circle.module.ts';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    UserRouting,
    LoadingCircleModule
  ],
  declarations: [
    UserComponent

  ],
  exports: [
    UserComponent
  ],
  providers: [
    UserService
  ]
})
export class UserModule {}
