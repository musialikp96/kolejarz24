import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';

import { UserComponent } from './user.component';

const userRoutes: Routes = [
  {
    path: 'user',
    component: UserComponent,
    children: [
      {
        path: '',
        component: UserComponent,
      },
      {
        path: 'historia',
        component: UserComponent,
      }
    ]
  }
];

export const UserRouting: ModuleWithProviders = RouterModule.forChild(userRoutes);
