/* jshint esversion: 6 */

var Sequelize = require('sequelize');
var sequelize = new Sequelize('kolejarz24', 'root', '', {
    host: 'localhost',
    port: 3306,
    ssl: false,
    dialect: 'mysql'
});

var Player = sequelize.define('player', {
    team: Sequelize.INTEGER,
    role: Sequelize.STRING,
    name: Sequelize.STRING,
    lastName: Sequelize.STRING,
    phoneNumber: Sequelize.INTEGER,
    address: Sequelize.STRING,
    firstClub: Sequelize.STRING,
    matches: Sequelize.INTEGER,
    minutes: Sequelize.INTEGER,
    goals: Sequelize.INTEGER,
    reds: Sequelize.INTEGER,
    yellows: Sequelize.INTEGER
});

module.exports = {
// tylko do tworzenia nowej bazy
    start: function(callback) {
        sequelize.sync({
            force: true
        }).then(function() {
            callback();
        });
    },

    getAllPlayers: function(callback) {
      Player.findAll().then(function(players) {
        callback(players);
      });
    },

    getPlayerById: function(id, callback) {
        Player.findAll({
            where: {
                id: id
            }
        }).then(function(player) {
            callback(player);
        });
    },

    getTeamById: function(id, callback) {
      Player.findAll({
        where: {
          team: id
        }
      }).then(function(players) {
        callback(players);
      });
    },

    addPlayer: function(player, callback) {
      Player.create({
        team: player.team,
        role: player.role,
        name: player.name,
        lastName: player.lastName,
        phoneNumber: player.phoneNumber,
        address: player.address,
        firstClub: player.firstClub,
        matches: player.matches,
        minutes: player.minutes,
        goals: player.goals,
        reds: player.reds,
        yellows: player.yellows
      }).then(function(player) {
        callback(player);
      });
    }
};
