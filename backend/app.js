var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');

var index = require('./routes/index');
var common = require('./routes/common');

var app = express();

app.options("*", cors()).use(cors());

// Set Static Folder
// app.use(express.static(path.join(__dirname, '../frontend/src')));

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(cookieParser());

app.use('/', index);
app.use('/common', common);

// Set Port
app.set('port', (process.env.PORT || 4000));

app.listen(app.get('port'), function() {
  console.log('Server started on port ' + app.get('port'));
});

module.exports = app;
