/* jshint esversion: 6 */
"use strict";
var express = require('express');
var request = require('request');
var cheerio = require('cheerio');
var database = require('../database/database');
var router = express.Router();

router.get('/getTable', function(req, res, next) {
    let url = 'https://www.laczynaspilka.pl/rozgrywki/nizsze-ligi,16715.html';
    request(url, function(error, resp, html) {
        if (!error) {
            var $ = cheerio.load(html);
            let arr = [];
            for (var i = 0; i < $('tbody tr').length; i++) {
                let obj = {
                    rank: 0,
                    team: "",
                    matches: 0,
                    won: 0,
                    draw: 0,
                    lost: 0,
                    goals: 0,
                    pts: 0
                };
                let str = [];
                for (var k = 2, z = 0; k <= $(`tbody tr.row-link:nth-child(1) td`).length; k++, z++) {
                    if (k != 3) {
                        str[z] = $(`tbody tr:nth-child(${i+1}) td:nth-child(${k})`).text();
                    } else {
                        z--;
                    }
                }
                obj.rank = str[0];
                obj.team = str[1];
                obj.matches = str[2];
                obj.won = str[3];
                obj.draw = str[4];
                obj.lost = str[5];
                obj.goals = str[6];
                obj.pts = str[7];
                arr[i] = obj;
            }
            res.json(arr);
        }
    });
});

router.get('/getTeam/:id', function(req, res, next) {
    let id = req.params.id;
    database.getTeamById(id, function(players) {
      res.json(players);
    });
    // if (id == 1)
    //     res.json([{
    //         "index": 0,
    //         "goals": 36,
    //         "minutes": 24,
    //         "name": "Jocelyn",
    //         "lastName": "Coffey",
    //         "position": "goalkeeper",
    //         "group": "goalkeepers"
    //     }, {
    //         "index": 1,
    //         "goals": 39,
    //         "minutes": 38,
    //         "name": "Rachelle",
    //         "lastName": "Meadows",
    //         "position": "defender",
    //         "group": "defenders"
    //     }, {
    //         "index": 2,
    //         "goals": 28,
    //         "minutes": 25,
    //         "name": "Tasha",
    //         "lastName": "Ware",
    //         "position": "defender",
    //         "group": "defenders"
    //     }, {
    //         "index": 3,
    //         "goals": 40,
    //         "minutes": 40,
    //         "name": "Terrell",
    //         "lastName": "Avery",
    //         "position": "midfielder",
    //         "group": "midfielders"
    //     }, {
    //         "index": 4,
    //         "goals": 21,
    //         "minutes": 36,
    //         "name": "Kristy",
    //         "lastName": "Solis",
    //         "position": "midfielder",
    //         "group": "midfielders"
    //     }, {
    //         "index": 5,
    //         "goals": 21,
    //         "minutes": 23,
    //         "name": "Delores",
    //         "lastName": "Torres",
    //         "position": "forward",
    //         "group": "forwards"
    //     }, {
    //         "index": 6,
    //         "goals": 29,
    //         "minutes": 29,
    //         "name": "Lottie",
    //         "lastName": "Rosales",
    //         "position": "forward",
    //         "group": "forwards"
    //     }]);
    // else if (id == 2)
    //     res.json([{
    //         "index": 0,
    //         "goals": 31,
    //         "minutes": 25,
    //         "name": "Nannie",
    //         "lastName": "Simmons",
    //         "position": "goalkeeper",
    //         "group": "goalkeepers"
    //     }, {
    //         "index": 1,
    //         "goals": 23,
    //         "minutes": 22,
    //         "name": "Avis",
    //         "lastName": "Reed",
    //         "position": "defender",
    //         "group": "defenders"
    //     }, {
    //         "index": 2,
    //         "goals": 20,
    //         "minutes": 38,
    //         "name": "Polly",
    //         "lastName": "Nguyen",
    //         "position": "midfielder",
    //         "group": "midfielders"
    //     }, {
    //         "index": 3,
    //         "goals": 21,
    //         "minutes": 28,
    //         "name": "Joy",
    //         "lastName": "Walters",
    //         "position": "midfielder",
    //         "group": "midfielders"
    //     }, {
    //         "index": 4,
    //         "goals": 21,
    //         "minutes": 27,
    //         "name": "Potter",
    //         "lastName": "Ramsey",
    //         "position": "forward",
    //         "group": "forwards"
    //     }, {
    //         "index": 5,
    //         "goals": 29,
    //         "minutes": 34,
    //         "name": "Acevedo",
    //         "lastName": "Jefferson",
    //         "position": "forward",
    //         "group": "forwards"
    //     }]);
    //
});

router.get('/getPlayer/:id', function(req, res, next) {
    let id = req.params.id;
    database.getPlayerById(id, function(player) {
      res.json(player);
    });
    // res.json({
    //     "index": 5,
    //     "goals": 29,
    //     "minutes": 34,
    //     "name": "Acevedo",
    //     "lastName": "Jefferson",
    //     "position": "forward",
    //     "group": "napastnicy"
    // });
});

router.post('/addPlayer/', function(req, res, next) {
    let player = req.body;
    let canAdd=true;
    database.getAllPlayers(function(players) {
      for(let i=0; i<players.length; i++){
        if(players[i].phoneNumber==player.phoneNumber){
          canAdd=false;
          break;
        }
      }

      if(!canAdd){
        res.json({
          success: false,
          information: "Taki zawodnik juz istnieje w bazie"
        });
      }
      else{
        database.addPlayer(player, function(player) {
          res.json({
            player
            // success: true,
            // information: "Dodano zawodnika"
          });
        });
      }
    });
});

module.exports = router;
